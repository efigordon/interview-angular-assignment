import { Pipe, PipeTransform } from '@angular/core';
/*
 * Remove non english characters.
 * Usage:
 *   value | myCustomPipe
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({name: 'myCustomPipe'})
export class MyCustomPipe implements PipeTransform {
  transform(value: string): string {
    let str = value;
    str = str.replace(/[^a-zA-Z 0-9]+/g, '')
    return str;
  }
}