import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { AppRoutingModule } from './app-routing.module';
import { MoviesComponent } from './movies/movies.component';
import { FormsModule } from "@angular/forms";
import { MovieService } from "./movie.service";
import { MyCustomPipe } from "./remove-non-english.pipe";
import { FlexLayoutModule } from "@angular/flex-layout";
import { DeleteMovieComponent } from "./movies/delete-movie.component";
import { EditMovieComponent } from './edit-movie/edit-movie.component';
import { HttpClientModule } from "@angular/common/http";
import { EditMovieManComponent } from './movies/edit-movie-manually.component';
import { AddMovieComponent } from './add-movie/add-movie.component';
import { CustomValidator } from "./custom-validator.directive";
import { CustomEditValidator } from './custom-validator-edit.directive';


@NgModule({
  declarations: [
    AppComponent,
    MoviesComponent,
    MyCustomPipe,
    DeleteMovieComponent,
    EditMovieComponent,
    EditMovieManComponent,
    AddMovieComponent,
    CustomValidator,
    CustomEditValidator
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    FormsModule,
    FlexLayoutModule,
    HttpClientModule,
    
  ],
  providers: [MovieService,],
  bootstrap: [AppComponent],
  entryComponents: [
    DeleteMovieComponent, 
    EditMovieComponent, 
    AddMovieComponent, 
    EditMovieComponent]
})
export class AppModule { }
