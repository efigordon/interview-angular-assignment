import { Injectable , Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IMovie } from './movie.interface';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private moviesList = [];
  private urls = [
    "http://www.omdbapi.com/?i=tt0462538&apikey=24b674a7",
    "http://www.omdbapi.com/?i=tt0264464&apikey=24b674a7",
    "http://www.omdbapi.com/?i=tt0172495&apikey=24b674a7",
    "http://www.omdbapi.com/?i=tt1022603&apikey=24b674a7",
    "http://www.omdbapi.com/?i=tt0814314&apikey=24b674a7",
    "http://www.omdbapi.com/?i=tt0111161&apikey=24b674a7",
    "http://www.omdbapi.com/?i=tt0126029&apikey=24b674a7",
    "http://www.omdbapi.com/?i=tt0109830&apikey=24b674a7",
    "http://www.omdbapi.com/?i=tt0317248&apikey=24b674a7",
    "http://www.omdbapi.com/?i=tt0120689&apikey=24b674a7",
    "http://www.omdbapi.com/?i=tt1675434&apikey=24b674a7",
  ];


  @Input() setMovies(list) {
    this.moviesList=[];
    this.moviesList = list;
  }
  constructor(private http: HttpClient) { }
  getMovies() {
    if(this.moviesList.length==0){
      for (let url in this.urls) {
        this.http.get<IMovie>(this.urls[url]).subscribe(data => {
          this.moviesList.push(data);
        });
      }
    }

    return this.moviesList;

  }
}
