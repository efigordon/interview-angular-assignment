import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material";

@Component({
    selector: 'app-edit-man-movie',
    template: `
    <div>
        
        <ul class="list-group">
            <li class="list-group-item"><p> Current Movie Details: </p></li>
            <li class="list-group-item list-group-item-primary">{{passedData.movie.Title}}</li>
            <li class="list-group-item list-group-item-secondary">{{passedData.movie.Year}} | {{passedData.movie.Runtime}}</li>
            <li class="list-group-item list-group-item-secondary"> {{passedData.movie.Genre}} | {{passedData.movie.Director}}</li>
        </ul>
    </div>
        <br>
    <div>
        <ul class="list-group">
            <li class="list-group-item"><h3>Edit: </h3>
            <p> Please fill all fields </p>
            </li>
        </ul>
    </div>
   
<div class="container-fluid mb-5">
    <form #userForm="ngForm">
        <div class="form-group">
            <label> Title: </label>
            <input type="text"  name="movieTitle" ngModel value="{{passedData.movie.Title}}"> 
        </div>
            <div class="form-group">
            <label> Year: </label>
            <input type="text"  name="movieYear" ngModel value="{{passedData.movie.Year}}"> 
        </div>
        <div class="form-group">
            <label> Runtime: </label>
            <input type="text"  name="movieRuntime" ngModel value="{{passedData.movie.Runtime}}"> 
        </div>
        <div class="form-group">
            <label> Genre: </label>
            <input type="text"  name="movieGenre" ngModel value="{{passedData.movie.Genre}}"> 
        </div>
        <div class="form-group">
            <label> Director: </label>
            <input type="text"  name="movieDirector" ngModel value="{{passedData.movie.Director}}"> 
        </div>
        
        {{userForm.value | json}}
    </form>
</div>
    <button type="submit" mat-raised-button [mat-dialog-close]="true" class="btn"> Save</button>
    <button mat-raised-button [mat-dialog-close]="false" class="btn"> Cancel </button>
    `
    })
export class EditMovieManComponent {
    constructor(@Inject(MAT_DIALOG_DATA) public passedData: any) {

    }
}