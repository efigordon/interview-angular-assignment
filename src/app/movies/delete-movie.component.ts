import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material";

@Component({
    selector: 'app-delete-movie',
    template: `
    <h1 mat-dialog-title> Are you Sure you want to delete
    <p> {{passedData.title}} </p> </h1>
    <mat-dialog-actions>
    <button mat-raised-button [mat-dialog-close]="true"> Yes</button>
    <button mat-raised-button [mat-dialog-close]="false"> No </button>
    </mat-dialog-actions>
    
    `
})
export class DeleteMovieComponent {
    constructor(@Inject(MAT_DIALOG_DATA) public passedData: any) {

    }

}